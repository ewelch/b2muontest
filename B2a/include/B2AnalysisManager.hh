 #ifndef B2AnalysisManager_h
 #define B2AnalysisManager_h 1
 
 #include "globals.hh"
 
 #include "TROOT.h"
 #include "TFile.h"
 #include "TTree.h"
 #include <vector>
 
 class B2AnalysisManager
 {
 public:
   virtual ~B2AnalysisManager();
  
 
   ///method to call to create an instance of this class
   static B2AnalysisManager* getInstance();
 
   void Book();
   void AddEvent(std::vector<G4double>, std::vector<G4int>, std::vector<G4double>, std::vector<G4double>, std::vector<G4double>, G4double);
   
   void CloseFile();
 
 
 private:
   static const Int_t MAX_DET=2000;
 
   ///private constructor in order to create a singleton
   B2AnalysisManager();
   static B2AnalysisManager* instance; 
 
   TFile* fFile;
   TTree* fTree;
 
   ///Store energy of the individual crystals
   Double_t fEnergy[MAX_DET];
 
   ///track length
   Double_t fTrackLength;

   //pdg code

   Int_t fPDGCode[MAX_DET];
 
 
   Int_t fNsecondaries;

   Double_t fpx[MAX_DET];
   Double_t fpy[MAX_DET];
   Double_t fpz[MAX_DET];

 };
 
 #endif
 
