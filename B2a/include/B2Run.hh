#ifndef B2Run_h
 #define B2Run_h 1
 
 #include "G4Run.hh"
 #include "globals.hh"
 #include <vector>
 
 /// Run class
 ///
  
 
 class B2Run : public G4Run
 {
 public:
   B2Run();
   virtual ~B2Run();
    void preRecordEvent(std::vector<G4double>, std::vector<G4int>, std::vector<G4double>, std::vector<G4double>, std::vector<G4double>, G4double);
   virtual void RecordEvent(const G4Event*);
   virtual void Merge(const G4Run*);
   std::tuple<std::vector<G4double>, std::vector<G4int>, std::vector<G4double>, std::vector<G4double>, std::vector<G4double>, G4double> getValues();
 private:
 
  
    G4double tracklength;
  std::vector<G4double> energy;
  
  
  std::vector<G4int> PDGCode;
  std::vector<G4double>px;
  std::vector<G4double>py;
  std::vector<G4double>pz;



  G4double stracklength;
  std::vector<G4double> senergy;
  
  
  std::vector<G4int> sPDGCode;
  std::vector<G4double> spx;
  std::vector<G4double> spy;
  std::vector<G4double> spz;

 };
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
 
 #endif
