#ifndef B2StackingAction_h
#define B2StackingAction_h 1
 
#include "G4UserStackingAction.hh"
#include "globals.hh"
#include "G4ClassificationOfNewTrack.hh"

 
 class B2StackingAction : public G4UserStackingAction
 {
   public:
     B2StackingAction();
     virtual ~B2StackingAction();
      
  

 protected: G4StackManager *stackManager;

 public:
virtual G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*);
   virtual void NewStage();
   virtual void PrepareNewEvent();

 };
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

 #endif
