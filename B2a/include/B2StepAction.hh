#ifndef B2StepAction_h
#define B2StepAction_h 1

#include "G4UserSteppingAction.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4ios.hh"
#include "globals.hh"

class G4Step;
class B2EventAction;
class B2StepAction: public G4UserSteppingAction
{
public:
   B2StepAction(B2EventAction*);
 ~B2StepAction();

 void UserSteppingAction(const G4Step* aStep);

private:
 

  G4TrackVector* fSecondary;
  B2EventAction* feventAction;
  G4double tracklength;
  std::vector<G4double> fenergy;
  
  
  std::vector<G4int> fPDGCode;
  std::vector<G4double> fpx;
  std::vector<G4double> fpy;
  std::vector<G4double> fpz;
 
};
#endif
