 #include "B2AnalysisManager.hh"
 #include "G4AutoLock.hh"
 #include "TError.h"
#include "G4Version.hh"
 #include "G4SystemOfUnits.hh"

 B2AnalysisManager* B2AnalysisManager::instance = 0;
 
 namespace { 
   //Mutex to acquire access to singleton instance check/creation
   G4Mutex instanceMutex = G4MUTEX_INITIALIZER;
   //Mutex to acquire accss to histograms creation/access
   //It is also used to control all operations related to histos 
   //File writing and check analysis
   G4Mutex dataManipulationMutex = G4MUTEX_INITIALIZER;
 }
 
 B2AnalysisManager::B2AnalysisManager() : 
   fFile(0),fTree(0)
 {;} 
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
 B2AnalysisManager::~B2AnalysisManager()
 {
  //No need to mutex, this is a real singleton.
   //loop over all histograms 
  if (fTree)
     delete fTree; 
   if (fFile) 
    delete fFile;
}

 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
  B2AnalysisManager* B2AnalysisManager::getInstance()
{
   G4AutoLock l(&instanceMutex);
   if (instance == 0) 
     instance = new B2AnalysisManager();
  return instance;
 }
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

 void B2AnalysisManager::Book()
 {
   //Booking of histograms has to be protected.
   //In addition there are issues with ROOT that is 
   //heavily non thread-safe. In particular I/O related operations
   //are not thread safe. To avoid problems mutex everything
   //here
   G4AutoLock l(&dataManipulationMutex);
   if (!fFile)
     {
       //create root file
       TString filename = "MTtest.root"; 
       fFile = new TFile(filename,"RECREATE");
     }
   //create tree and branches
   if (!fTree)
     {
    fTree = new TTree("tree","Global results");
    fTree->Branch("Nsecondaries",&fNsecondaries,"Nsecondaries/I");
    fTree->Branch("Energy_GeV",fEnergy,"Energy_GeV[Nsecondaries]/D");
    fTree->Branch("Muon_track_length_m",&fTrackLength,"Muon_track_length_m/D");
    fTree->Branch("PDGcode",fPDGCode,"PDGcode[Nsecondaries]/I");
    fTree->Branch("px",fpx,"px[Nsecondaries]/D");
    fTree->Branch("py",fpy,"py[Nsecondaries]/D");
    fTree->Branch("pz",fpz,"pz[Nsecondaries]/D");
  
     }
   return;
 }
 
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
 
void B2AnalysisManager::AddEvent(std::vector<G4double> ene, std::vector<G4int> pdgc, std::vector<G4double> ppx, std::vector<G4double> ppy, std::vector<G4double> ppz, G4double tlength)
 {
   
   G4AutoLock l(&dataManipulationMutex);

   //fNsecondaries is to allow proper vector lengths for other variables
   fNsecondaries = (Int_t) ene.size();
   //take info from addEvent input
for (size_t i=0;i<ene.size();i++)    
     {
fEnergy[i] = ene.at(i);
fPDGCode[i] = pdgc.at(i);
 fpx[i] = ppx.at(i);
 fpy[i] = ppy.at(i);
 fpz[i] = ppz.at(i);

     }

fTrackLength =(Double_t) tlength;

 //fill the tree
   fTree->Fill();
 }
 




 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...


 //close root file
 void B2AnalysisManager::CloseFile()
 {
   G4AutoLock l(&dataManipulationMutex);
   if (!fFile) //file not created at all: e.g. for a vis-only execution
     return;
   if (!fFile->IsOpen())
     {
       G4Exception("B2AnalysisManager::CloseFile()","tst67_02",FatalException,
                   "Trying to close a ROOT file which is not open");
       return;
     }
   fFile->cd(); 
   if (fTree)
    fTree->Write(fTree->GetName());
   fFile->Close();
 } 


