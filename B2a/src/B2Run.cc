#include "B2Run.hh"
 #include "G4AutoLock.hh"
 #include "G4RunManager.hh"
 #include "G4Event.hh"
 
 #include "G4SDManager.hh"
 #include "G4HCofThisEvent.hh"
 #include "G4THitsMap.hh"
 #include "G4SystemOfUnits.hh"
 #include "tuple"
 #include "B2AnalysisManager.hh"
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
namespace {
  G4Mutex variableMutex =G4MUTEX_INITIALIZER;
}
 
 B2Run::B2Run()
   : G4Run(),stracklength(0.),senergy(0.),sPDGCode(0.),spx(0.),spy(0.),spz(0.),preRecordEvent(fenergy,fPDGCode,fpx,fpy,fpz,ftracklength)
 { }
 
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
 B2Run::~B2Run()
 { }
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


void B2Run::preRecordEvent(std::vector<G4double> fenergy, std::vector<G4int> fPDGCode, std::vector<G4double> fpx, std::vector<G4double> fpy, std::vector<G4double> fpz, G4double ftracklength)
{
  G4AutoLock l(&variableMutex);
 energy=fenergy;
 PDGCode=fPDGCode;
  px=fpx;
  py=fpy;
 pz=fpz;
 tracklength=ftracklength;
 

}

std::tuple<std::vector<G4double>, std::vector<G4int>, std::vector<G4double>, std::vector<G4double>, std::vector<G4double>, G4double> getValues()
{
    // Packing values to return a tuple
  return std::make_tuple(energy,PDGCode,px,py,pz,tracklength);             
}

 /// Called at the end of each event
void B2Run::RecordEvent(const G4Event* event)  
{

 G4AutoLock l(&variableMutex);
  senergy.clear();
  sPDGCode.clear();
  spx.clear();
  spy.clear();
  spz.clear();

  std::tie(senergy,sPDGCode,spx,spy,spz,stracklength)=getValues();

 std::cout<<stracklength;
  B2AnalysisManager::getInstance()->AddEvent(senergy,sPDGCode,spx,spy,spz,stracklength);

  G4Run::RecordEvent(event);
}

 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
 void B2Run::Merge(const G4Run* aRun)
 {
  
   //do the merge of the information coming from the individual threads.
   
 
   G4Run::Merge(aRun); 
 } 
 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


