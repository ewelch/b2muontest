#include "B2StepAction.hh"
#include "G4SteppingManager.hh"
#include "G4Step.hh"
#include "B2RunAction.hh"
#include "B2EventAction.hh"
#include "G4Run.hh"
#include "B2AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4ParticleDefinition.hh"
#include "TTree.h"
#include "G4UImanager.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "B2Run.hh"
#include "G4Event.hh"
#include<cmath>



B2StepAction::B2StepAction(B2EventAction* eventAction)
  :G4UserSteppingAction(),feventAction(eventAction),tracklength(0.),fenergy(0.),fPDGCode(0.),fpx(0.),fpy(0.),fpz(0.)
{
 
}

B2StepAction::~B2StepAction()
{
}


void B2StepAction::UserSteppingAction(const G4Step* aStep)
{
  //reset variables (besides muon track length) for each step

  fenergy.clear();
  fPDGCode.clear();
  fpx.clear();
  fpy.clear();
  fpz.clear();

  //set flag to false-flag to tell if secondar>emin was produced
  bool flag = false;

  //minimum secondary energy to be recorded
  static G4double eMin = .01 *GeV;
 


  //G4SteppingManager* steppingManager = fpSteppingManager;

 G4Track* theTrack = aStep->GetTrack();
 
 //if(theTrack->GetTrackStatus()==fAlive){return;}

 




if(theTrack->GetParentID()==0)
  {
   
    tracklength =(theTrack->GetTrackLength())/1000;

}






 
// tracklength = sqrt(pow(pos[0],2)+pow(pos[1],2)+pow((pos[2]+60),2));


//muon track length

//tracklength =  feventAction->RetrieveTrackLength();
  
 //retrieve secondary particle info
 // G4TrackVector* fsecondary = steppingManager-> GetfSecondary();
 auto fsecondary = aStep->GetSecondaryInCurrentStep();
 // if ((*fsecondary).size()>0)
 // {


 //loop through each secondary
  for (size_t lp1 =0; lp1<(*fsecondary).size();lp1++)
    {
      
      //determine if secondary is has min kinetic energy
      if (((*fsecondary)[lp1]->GetKineticEnergy())>=eMin)
	{
	  //retrieve variables for  secondaries
 G4int code = (*fsecondary)[lp1]->GetParticleDefinition()->GetPDGEncoding();
 G4double energy = ((*fsecondary)[lp1]->GetKineticEnergy())/1000;
 G4double secondaryDirectionX = (*fsecondary)[lp1]->GetMomentumDirection()[0];
 G4double secondaryDirectionY = (*fsecondary)[lp1]->GetMomentumDirection()[1];
 G4double secondaryDirectionZ = (*fsecondary)[lp1]->GetMomentumDirection()[2];

 //add variable to respective vectors for each secondary
  fPDGCode.push_back(code);
      fenergy.push_back(energy);
      fpx.push_back(secondaryDirectionX);
      fpy.push_back(secondaryDirectionY);
      fpz.push_back(secondaryDirectionZ);
     
      //make flag true to signal the tree to be filled
  flag = true;   

	}
    }
  // G4int pid = theTrack->GetParentID();

  //secondary with energy greater than emin created, so fill the tree and abort event to move on to next muon
 
  
      if (flag == true)
	{
	  
	  B2Run obj;
	  obj.preRecordEvent(fenergy,fPDGCode,fpx,fpy,fpz,tracklength);

	  // B2AnalysisManager::getInstance()->AddEvent(fenergy,fPDGCode,fpx,fpy,fpz,tracklength);
	 

        
 G4UImanager* UImanager = G4UImanager::GetUIpointer();
 UImanager->ApplyCommand("/event/abort");
	}
      // }




}


